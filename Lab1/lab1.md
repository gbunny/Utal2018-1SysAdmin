# Laboratorio 1: Configuración basica
## **Autor: Alan Lisboa**

### Topologia

![](./Topologia.png)


|Dispositivo|Interfaz|IP|Mask|Gateway|
|-|-|-|-|-|
|Router0|Gig0/0|192.168.1.1|255.255.255.0| N/C |
| - | S0/0/0 |192.168.2.1 | 255.255.255.0 | N/C |
|Router1|Gig0/0|192.168.2.1| 255.255.255.0 | N/C |
| - |S0/0/0 |192168.2.2 | 255.255.255.0 | N/C|
|PC0|Fa0/0| 192.168.1.10|255.255.255.0|192.168.1.1|
|PC1|Fa0/0|192.168.3.10|255.255.255.0|192.168.3.1|

> ¿Cuáles de los dispositivos del Diagrama de topología requieren un cable Ethernet entre ellos?

- PC <-> Switch
- PC <-> Router
- Switch <-> Router

> ¿De qué color es la luz de estado de enlace que se encuentra junto a la interfaz FastEthernet 0/0 en R1?

- Rojo

> ¿De qué color es la luz de estado de enlace que se encuentra junto a la interfaz FastEthernet 0/1 en S1?

- Rojo

> ¿De qué color es la luz de estado de enlace que se encuentra junto a la interfaz NIC en PC1?

- Verde

>¿De qué color es la luz de estado de enlace que se encuentra junto a la interfaz FastEthernet 0/2 en S1?

- Verde

> ¿De qué color es la luz de estado de enlace que se encuentra junto a la interfaz NIC en la PC2?

- Rojo

> ¿De qué color es la luz de estado de enlace que se encuentra junto a la interfaz FastEthernet 0/0 en R2?

- Rojo

### Tarea 4

![](./P1&2.png)

### Tarea 5

![](./P3.png)

![](./T51.png)

![](./T52.png)

![](./T53.png)

### Tarea 6

![](./T61.png)

> ¿Por qué desearía deshabilitar la búsqueda de DNS en un entorno de laboratorio?

- Reduce el trabajo necesario para decifrar una direccion.

> ¿Qué sucedería si se deshabilitara la búsqueda de DNS en un ambiente de producción?

- Se puede perder la capacidad de decifrar direcciones no ip, como por ejemplo google.com

![](./T62.png)

> ¿Cuándo se muestra este título?

- Al iniciar una nueva sesion con el aparato.

> ¿Por qué todos los routers deben tener un título con el mensaje del día?

- Notifican información legal de acceso y/o utilidad.

![](./T63.png)

![](./T64.png)

### Tarea 7

![](./T71.png)


![](./T72.png)

### Tarea 8

![](./T81.png)

![](./T82.png)

### Tarea 9

> Paso 1

```

R0#show running-config
Building configuration...

Current configuration : 1034 bytes
!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname R0
!
!
!
enable secret 5 $1$mERr$9cTjUIEqNGurQiFU.ZeCi1
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO1941/K9 sn FTX152466EP
!
!
!
!
!
!
!
!
!
no ip domain-lookup
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface GigabitEthernet0/0
 description R0 LAN
 ip address 192.168.1.1 255.255.255.0
 duplex auto
 speed auto
!
interface GigabitEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/0/0
 description Link to R1
 ip address 192.168.2.1 255.255.255.0
 clock rate 64000
!
interface Serial0/0/1
 no ip address
 clock rate 2000000
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
!
banner motd ^C
********************************

!!!AUTHORIZED ACCESS ONLY!!!

********************************

^C
!
!
!
!
line con 0
 password cisco
 login
!
line aux 0
!
line vty 0 4
 password cisco
 login
!
!
!
end


R0#
```

> Paso 2

```

R0#show startup-config
Using 1034 bytes
!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname R0
!
!
!
enable secret 5 $1$mERr$9cTjUIEqNGurQiFU.ZeCi1
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO1941/K9 sn FTX152466EP
!
!
!
!
!
!
!
!
!
no ip domain-lookup
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface GigabitEthernet0/0
 description R0 LAN
 ip address 192.168.1.1 255.255.255.0
 duplex auto
 speed auto
!
interface GigabitEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/0/0
 description Link to R1
 ip address 192.168.2.1 255.255.255.0
 clock rate 64000
!
interface Serial0/0/1
 no ip address
 clock rate 2000000
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
!
banner motd ^C
********************************

!!!AUTHORIZED ACCESS ONLY!!!

********************************

^C
!
!
!
!
line con 0
 password cisco
 login
!
line aux 0
!
line vty 0 4
 password cisco
 login
!
!
!
end
```

> Paso 3

![](./T91.png)

> Paso 4

```
R0#show version
Cisco IOS Software, C1900 Software (C1900-UNIVERSALK9-M), Version 15.1(4)M4, RELEASE SOFTWARE (fc2)
Technical Support: http://www.cisco.com/techsupport
Copyright (c) 1986-2007 by Cisco Systems, Inc.
Compiled Wed 23-Feb-11 14:19 by pt_team

ROM: System Bootstrap, Version 15.1(4)M4, RELEASE SOFTWARE (fc1)
cisco1941 uptime is 33 minutes, 23 seconds
System returned to ROM by power-on
System image file is "flash0:c1900-universalk9-mz.SPA.151-1.M4.bin"
Last reload type: Normal Reload

This product contains cryptographic features and is subject to United
States and local country laws governing import, export, transfer and
use. Delivery of Cisco cryptographic products does not imply
third-party authority to import, export, distribute or use encryption.
Importers, exporters, distributors and users are responsible for
compliance with U.S. and local country laws. By using this product you
agree to comply with applicable laws and regulations. If you are unable
to comply with U.S. and local laws, return this product immediately.

A summary of U.S. laws governing Cisco cryptographic products may be found at:
http://www.cisco.com/wwl/export/crypto/tool/stqrg.html

If you require further assistance please contact us by sending email to
export@cisco.com.
Cisco CISCO1941/K9 (revision 1.0) with 491520K/32768K bytes of memory.
Processor board ID FTX152400KS
2 Gigabit Ethernet interfaces
2 Low-speed serial(sync/async) network interface(s)
DRAM configuration is 64 bits wide with parity disabled.
255K bytes of non-volatile configuration memory.
249856K bytes of ATA System CompactFlash 0 (Read/Write)

License Info:

License UDI:

-------------------------------------------------
Device#   PID                   SN
-------------------------------------------------
*0        CISCO1941/K9          FTX152466EP


Technology Package License Information for Module:'c1900'

----------------------------------------------------------------
Technology    Technology-package          Technology-package
              Current       Type          Next reboot
-----------------------------------------------------------------
ipbase        ipbasek9      Permanent     ipbasek9
security      disable       None          None
data          disable       None          None

Configuration register is 0x2102
```

> Paso 5

![](./T92.png)

### Tarea 10

![](./T101.png)

![](./T102.png)

### Tarea 11

![](./T111.png)

![](./T112.png)

### Tarea 12

> Paso 1

- Ver start_1.txt

> Paso 2

- Ver start.txt

### Tarea 13


