# Laboratorio 3: Wireshark
**Autor: Alan Lisboa**

## Parte 2

> Paso 1

Origen
![](./Interface.PNG)

Destino
![](./InterfaceDest.png)

> Paso 2

![](./Ping1.PNG)

![](./wire1.PNG)

> Paso 3

![](./macsource.PNG)

- ¿La dirección MAC de origen coincide con la interfaz de su PC?

Si, la misma direccion fisica aparece en ipconfig/all y wireshark

- ¿La dirección MAC de destino en Wireshark coincide con la dirección MAC del miembro del equipo?

Si.

- ¿De qué manera su PC obtiene la dirección MAC de la PC a la que hizo ping?

Es parte del datagrama.

## Parte 3

> Paso 2   

| ubicacion | ip | mac |
| --- | --- | --- |
| www.yahoo.com | 72.30.35.10 | 98:e7:f5:bc:f5:db |
| www.cisco.com | 23.41.157.13 | 98:e7:f5:bc:f5:db |
| www.google.com | 64.233.186.104 | 98:e7:f5:bc:f5:db |

![](./yahoo.PNG)

![](./cisco.PNG)

![](./google.PNG)

- b. ¿Qué es importante sobre esta información?

La IP es la direccion en el internet, y la mac el indentificador unico fisico.

- c. ¿En qué se diferencia esta información de la información de ping local que recibió en la parte 2?

La mac es la misma para todos los dominios.

- c. ¿En qué se diferencia esta información de la información de ping local que recibió en la parte 2?

Por que el datagrama tiene la ubicación del router en ves de la maquina que respondio.

> Paso 3

- a) Identifique cual es la mac de destino y origen, en un paquete que hayas enviado

![](./Macs.PNG)

- b) Busque el paquete donde se solicita permiso para ingresar y muestre los bits dondese almacena el nombre del usuario

![](./usersend.PNG)

- c) Dado que Wireshark captura todos los paquetes enviados y recibidos busque el paquete donde recibió el archivo, luego ve en el paquete que contenía el archivo, y comprueba que este tiene el mismo contenido que el archivo descargado

![](./fileShark.PNG)

![](./file.PNG)

> Paso 4

- a) Dirección IP de destino

![](./utalca.PNG)

- b) Compruebe que esta es la correcta mediante un ping

![](./utalcaping.PNG)

![](./navegador.PNG)

- c) Dirección IP de la fuente

R: 192.168.1.4

- d) ¿Cuál es el Host de destino?

R: www.utalca.cl

- e) ¿Qué explorador/versión muestra el paquete? ¿Coincide con el suyo?

R: Si, visite www.utalca.cl mediante waterfox 56, un fork de mozilla.

- f) Identifique la porción de números hexadecimales que corresponden al host, explorador y mac de los dispositivos. 




