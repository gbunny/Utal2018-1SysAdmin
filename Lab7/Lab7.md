# Laboratorio 7: NAT

**Nombre**: Alan Lisboa

## Configuracion inicial

![Topografia](topografia.png)

|Dispositivo|Interfaz|Ip|Mask|Gw|
|-----------|--------|--|----|--|
|Gateway | - | - | - |
| | S0/1/1 | 209.165.201.18 | 255.255.255.252 | N/A |
| | G0/0 | 192.168.1.1 | 255.255.255.0 | N/A|
| ISP |  - | - | - |
| | S0/1/0 | 209.165.201.17 | 255.255.255.252 | N/A |
| | G0/0 | 192.31.7.1 | 255.255.255.0 | N/A |
| PCA |  F0/0 | 192.168.1.20 | 255.255.255.0 | 192.168.1.1|
| PCB |  F0/0 | 192.168.1.21 | 255.255.255.0 | 192.168.1.1|
| Server | F0/0 | 192.31.7.2 | 255.255.255.0 | 192.31.7.1 |
 

## Paso 3

### A

![Tabla NAT](3-a.png)

> ¿Cuál es la traducción de la dirección host local interna? 192.168.1.20 =  

- 209.165.200.225

> ¿Quién asigna la dirección global interna?  

- Yo/el Administrador, durante el static nat source.

> ¿Quién asigna la dirección local interna?  

- Similar a la respuesta anterior, fue asignada durante el static nat source.

### B

![Tabla-Nat](3-b.png)


> ¿Qué número de puerto se usó en este intercambio ICMP?  
 
- 15

### C

![Tabla-Nat](3-c.png)


> ¿Qué protocolo se usó para esta traducción?  

- tcp

> ¿Cuáles son los números de puerto que se usaron?  

- 1025 y 23

> Global/local interno:  

- 209.165.200.225:1025, 192.168.1.20:1025

> Global/local externo:  

- 192.31.7.1:23, 192.31.7.1:23

### D
![Ping:Server->PCA](3-d.png)


### E
![TablaNat](3-e.png)


### F

![TablaNat](3-f.png)


## Paso 6

### A
![tablaNat](6-a.png)

> ¿Cuál es la traducción de la dirección host local interna de la PC-B? 192.168.1.21  

- 209.165.200.242

> ¿Qué número de puerto se usó en este intercambio ICMP?  

- 3

### B

> ¿Qué protocolo se usó en esta traducción?  

- tcp

> ¿Qué protocolo se usó en esta traducción?  

- (internal) 1025 y (external)  80

> ¿Qué número de puerto bien conocido y qué servicio se usaron?  

- http (80)

### D

![tablaNat](6-d.png)


## Paso 7

![stats](7-d.png)

![std](7-d2.png)


## Reflexión

>  ¿Por qué debe utilizarse la NAT en una red?  

- NAT separa las ip de la red interna con la de las externas. Permitiendo que la misma ip pueda ser utilizada en distintas redes y ademas estas redes puedan comunicarse entre si.

> ¿Cuáles son las limitaciones de NAT?  

- Nat requiere la sincronización de las ips externas entre las redes.

