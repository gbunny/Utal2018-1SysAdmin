# Laboratorio 8

## Administracion de Procesos

**Nombre**:Alan Lisboa

1. Use ps para buscar el proceso init por su nombre.  
```
$ps -awx
1 ?        Ss     0:19 /usr/lib/systemd/systemd --switched-root --system --deserialize 24
```
2. ¿Cuál es el process id (pid) del proceso init?

- R: 1

3. Use el comando who am i para determinar el nombre de su terminal

```
$whoami
gbunny
```
4. Usando el nombre de su terminal obtenido anteriormente, use ps para encontrar
todos los procesos asociados a tu terminal.
```
$ps -u gbunny
  PID TTY          TIME CMD
 1287 ?        00:00:04 baloo_file
 6500 ?        00:00:00 systemd
 6502 ?        00:00:00 (sd-pam)
 6513 ?        00:00:00 kwalletd5
 6516 ?        00:00:00 startkde
 6571 ?        00:00:00 dbus-daemon
 6582 ?        00:00:00 ssh-agent
 6621 ?        00:00:00 start_kdeinit
 6622 ?        00:00:00 kdeinit5
 6623 ?        00:00:00 klauncher
 6626 ?        00:00:02 kded5
 6653 ?        00:00:00 kaccess
 6656 ?        00:00:00 kwrapper5
 6658 ?        00:00:00 ksmserver
 6666 ?        00:00:00 kglobalaccel5
 6673 ?        00:00:00 dconf-service
 6679 ?        00:00:51 kwin_x11
 6681 ?        00:00:00 baloo_file
 6686 ?        00:00:00 kdeconnectd
 6688 ?        00:00:01 krunner
 6690 ?        00:00:14 plasmashell
 6699 ?        00:00:00 polkit-kde-auth
 6701 ?        00:00:00 xembedsniproxy
 6726 ?        00:00:00 kactivitymanage
 6746 ?        00:00:17 pulseaudio
 6761 ?        00:00:00 mission-control
 6763 ?        00:00:00 kscreen_backend
 6814 ?        00:00:00 abrt-applet
 6829 ?        00:00:00 org_kde_powerde
 6833 ?        00:00:00 at-spi-bus-laun
 6839 ?        00:00:00 dbus-daemon
 6844 ?        00:00:00 at-spi2-registr
 6921 ?        00:00:00 file.so
 6927 ?        00:00:00 kuiserver5
 6937 ?        00:00:00 ksysguardd
 6945 ?        00:01:42 firefox
 7015 ?        00:03:43 Web Content
 7065 ?        00:00:05 konsole
 7069 ?        00:00:00 gconfd-2
 7086 pts/1    00:00:00 zsh
 7597 ?        00:00:13 FoxitReader
 7731 ?        00:00:24 code
 7739 ?        00:00:00 code
 7764 ?        00:00:16 code
 7779 ?        00:00:02 code
 7929 ?        00:00:46 code
 7978 ?        00:00:04 code
 7985 ?        00:00:00 code
 8243 pts/1    00:00:00 ps
19456 ?        00:00:00 kio_http_cache_
```
- R: pts/1

6. ¿Cuál es el process id del proceso al que está asociado su shell?
- R: 7086

7. Inicie dos instancias de sleep 3342 tras bastidores (background).
```
$ sleep 3342 &
[1] 8465
$ sleep 3342 &
[2] 8472
```

8. Localice el process id de todos los comandos sleep.
- Vease 7
```
$ps l
F   UID   PID  PPID PRI  NI    VSZ   RSS WCHAN  STAT TTY        TIME COMMAND
0  1000  7086  7065  20   0 163424  6436 -      Ss   pts/1      0:00 /bin/zsh
0  1000  8465  7086  25   5 114684   780 hrtime SN   pts/1      0:00 sleep 3342
0  1000  8472  7086  25   5 114684   708 hrtime SN   pts/1      0:00 sleep 3342
0  1000  8516  7086  20   0 143048  1744 -      R+   pts/1      0:00 ps l
```

9. Muestre solo aquellos dos procesos sleep en top. Luego termine top.
```
$top -p 8465 -p 8572

top - 11:35:43 up 20:31,  2 users,  load average: 0.33, 0.78, 1.04
Tasks:   1 total,   0 running,   1 sleeping,   0 stopped,   0 zombie
%Cpu(s):  2.3 us,  1.3 sy,  1.3 ni, 94.1 id,  0.5 wa,  0.3 hi,  0.3 si,  0.0 st
KiB Mem :  3931020 total,   215684 free,  1559608 used,  2155728 buff/cache
KiB Swap: 10022908 total,  9964832 free,    58076 used.  2101728 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND                                                                                         
 8465 gbunny    25   5  114684    780    716 S   0.0  0.0   0:00.00 sleep           
```

10. Use un kill standard para terminar (matar) uno de los procesos sleep.
```
$kill 8465
[1]    8465 terminated  sleep 3342    
```

11. Use un solo comando kill para terminar con todos los procesos sleep.
```
$ killall sleep
[2]  - 8472 terminated  sleep 3342
```

## Procesos en background

1. Use el comando jobs para verificar si usted tiene algún proceso corriendo en
background.

```
$ jobs
[3]  + suspended  vim text.txt
```

2. Use vi para crear un pequeño archivo de texto. Deje suspendido vi en background.
```
$ vi nuevotexto.txt
```
![Vi](Vi.png)
```
CTRL-Z
[1]  + 8804 suspended  vim nuevotexto.txt
```

3. Verifique, usando el comando jobs que vi está suspendido en background.
```
$ jobs
[1]  + suspended  vim nuevotexto.txt
[3]  - suspended  vim text.txt
```

4. Inicie find / > allfiles.txt 2>/dev/null en foreground y luego suspéndalo en background
antes de que termine.

```
$  find / > allfiles.txt 2>/dev/null
[2]  + 8916 suspended  find / > allfiles.txt 2> /dev/null
```

5. Inicie dos procesos sleep largos (long sleep) en background.
```
$ sleep 5d &
[4] 8981
$ sleep 8d &
[5] 8988
```

![Double Duble](doubledouble.png)


6. Muestre todos los jobs en background.
```
$ jobs

[1]  - suspended  vim nuevotexto.txt
[2]  + suspended  find / > allfiles.txt 2> /dev/null
[3]    suspended  vim text.txt
[4]    running    sleep 5d
[5]    running    sleep 8d
```

7. Use el comando kill para suspender el último proceso sleep.
```
$ kill %5
[5]    8988 terminated  sleep 8d                   
```

```
$ kill -STOP %4                            
[4]  + 8981 suspended (signal)  sleep 5d                             
```

8. Continúe el proceso find, que dejamos en background, y verifique que continuo su
ejecución.
![continue](Continue2.png)

9. Traiga unos de los comandos sleep de vuelta al foreground.
![foreground](foreground.png)