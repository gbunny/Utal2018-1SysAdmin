# Laboratorio 9
**Autor: Alan Lisboa**

1. Ver la vesión de iptables.

`iptables -V`
![](./L1.png)

2. Borrado de todas las reglas.

`iptables -F`

![](./L2.png)

3. Añadir una regla a la cadena INPUT para aceptar todos los paquetes que se originan desde la dirección 192.168.0.155.

`iptables -A INPUT -s 192.168.0.155 -j ACCEPT`

![](./L3.png)


4. Descartar todos los paquetes que entren.

`iptables -A INPUT -j DROP`

![](./L4.png)

5. Permitir la salida de paquetes.

`iptables -A OUTPUT -j ACCEPT`

![](,/L5.png)

6. Añadir una regla a la cadena INPUT para rechazar todos los paquetes que se originan desde la dirección 192.168.0.155.

`iptables -A INPUT -s 192.168.0.155 -j REJECT`
![](./L6.png)

7. Añadir una regla a la cadena INPUT para rechazar todos los paquetes que se
originan desde la dirección de red 192.168.0.0.

`iptables -A INPUT -s 192.168.0.0/24 -j REJECT`
![](./L7.png)

8. Añadir una regla a la cadena INPUT para rechazar todos los paquetes que se originan desde la dirección 192.168.0.155 y enviar un mensaje de error icmp.

`iptables -A INPUT -s 192.168.0.155 -j REJECT --reject-with icmp-host-prohibited`
![](./L8.png)

9. Permitir conexiones locales (al localhost), por ejemplo, a mysql.

`iptables -A INPUT -s 127.0.0.1 -p tcp --dport 1433 -j ACCEPT`
![](./L9.png)

10. Permitir el acceso a nuestro servidor web (puerto TCP 80).

`iptable -A INPUT -p tcp --dport 80 -j ACCEPT`
![](./L10.png)

11. Permitir el acceso a nuestro servidor ftp (puerto TCP 20 y 21).

`iptables -A INPUT -p tcp --dport 20:21 -j ACCEPT`
![](./L11.png)

12. Permitir a la máquina con IP 192.168.0.155 conectarse a nuestro equipo a través
de SSH.

`iptables -A INPUT -s 192.168.0.155 -p tcp --dport 22 -j ACCEPT`

13. Rechazar a la máquina con IP 192.168.0.155 conectarse a nuestro equipo a
través de Telnet.

`iptables -A INPUT -s 192.168.0.155 -p tcp --dport 25 -j REJECT`

14. Rechazar las conexiones que se originen de la máquina con la dirección física 00:db:f0:34:ab:78.



15. Rechazar todo el tráfico que ingrese a nuestra red LAN 192.168.0.0 /24 desde una red remota, como Internet, a través de la interfaz eth0.

`iptables -A INPUT -i eth0 -d 192.168.0.0/24 -j REJECT`


16. Cerrar el rango de puerto bien conocido desde cualquier origen (puertos de 1 al
1024).

`iptables -A INPUT -p tcp --dport 1:1024 -j DROP`
`iptables -A INPUT -p upd --dport 1:1024 -j DROP`


17. Aceptar que vayan de nuestra red 192.168.0.0/24 a un servidor web (puerto 80).

`iptables -A INPUT -s 192.168.0.0/24 -p tcp --dport 80 -j ACCEPT`

18. Aceptar que nuestra LAN 192.168.0.0/24 vaya a puertos https (puerto 443).

`iptables -A INPUT -s 192.168.0.0/24 -p tcp --dport 443 -j ACCEPT`

19. Aceptar que los equipos de nuestra red LAN 192.168.0.0/24 consulten los DNS,
y denegamos el resto a nuestra red.

`iptables -A INPUT -s 192.168.0.0/24 -p upd --dport 53 -j ACCEPT`
`iptables -A INPUT -s 192.168.0.0/24 -p tcp --dport 53 -j ACCEPT`
`iptables -A INPUT -s 192.168.0.0/24 -j REJECT`

20. Permitir enviar y recibir e-mail a todos (puerto 25).

`iptables -A INPUT -p tcp --dport 25 -j ACCEPT`
`iptables -A OUTPUT -p tcp --dport 25 -j ACCEPT`

21. Cerrar el acceso de una red definida 192.168.3.0/24 a nuestra red LAN 192.168.2.0/24.

`iptables -A FORWARD -s 192.168.3.0/24 -d 192.168.2.0/24 -j DROP`

22. Permitir el paso de un equipo específico 192.168.3.5 a un servicio (puerto 5432 que ofrece un equipo específico (192.168.0.5) y su respuesta.

`iptables -A FORWARD -s 192.168.3.5 -d 192.168.0.5 -p tcp --dport 5432 -j ACCEPT`

23. Permitir el paso de paquetes cuya conexión ya se ha establecido o es nueva
pero está relacionada a una conexión ya establecida.



