# Laboratorio 2: Configuración Enrutamiento
## **Autor: Alan Lisboa**

![](./Topografia.png)


|Dispositivo|Interfaz|IP|Mask|Gateway|
|-|-|-|-|-|
|Router0|Gig0/0|192.168.1.1|255.255.255.0| N/C |
| - | S0/0/0 |192.168.2.1 | 255.255.255.0 | N/C |
|Router1|Gig0/0|192.168.2.1| 255.255.255.0 | N/C |
| - |S0/0/0 |192168.2.2 | 255.255.255.0 | N/C|
|PC0|Fa0/0| 192.168.1.10|255.255.255.0|192.168.1.1|
|PC1|Fa0/0|192.168.3.10|255.255.255.0|192.168.3.1|

### Tarea 1

> ¿Qué tipo de cable se utiliza para conectar la interfaz Ethernet de una PC host a la interfaz Ethernet de un switch?

- Ethernet, directo de cobre.

> ¿Qué tipo de cable se utiliza para conectar la interfaz Ethernet de un switch a la interfaz Ethernet de un router?

- Ethernet, directo de cobre.

> ¿Qué tipo de cable se utiliza para conectar la interfaz Ethernet de un router a la interfaz Ethernet de una PC host?

- Ethernet, A travez de cobre.

### Tarea 2

> ¿Qué pasaría si respondiera sí a la pregunta: "System configuration has been modified. Save?" (La configuración del sistema ha sido modificada. ¿Desea guardarla?)?

- La configuración actual se guardará y no se cargaria la configuración de fabrica.


### Tarea 3

> ¿Por qué desearía deshabilitar la búsqueda de DNS en un entorno de laboratorio?

- Para no gastar tiempo buscando ip fuera de la red.

> ¿Qué sucedería si se deshabilitara la búsqueda de DNS en un ambiente de producción?

- El router no podria decifrar la ip de los dominios.

> ¿Por qué no es necesario utilizar el comando enable password password?

- Es preferido tener una password habilitada. Por lo cual el OS es suficientemente inteligente para habilitar el password en cuanto pueda, si es que no existen comandos diciendo lo contrario.

> ¿Cuándo se muestra este título?

- Al iniciar una session con el SO del router.

> ¿Por qué todos los routers deben tener un título con el mensaje del día?

- Muestran informacion legal de acceso y/o utilidad.

![](./T31.png)

![](./T32.png)

> ¿Cuál es la versión más corta de este comando? 

- copy r s

### Tarea 4

![](./T41.png)

![](./T42.png)

### Tarea 5

![](./T51.png)

![](./T52.png)

### Tarea 6

![](./T61.png)

![](./T62.png)

![](./T63.png)

![](./T64.png)

> Paso 3

![](./T65.png)

> Paso 4

![](./T66.png)

![](./T67.png)

### Tarea 7

> ¿Qué es lo que falta en la red que impide la comunicación entre estos dispositivos?

- Los routers no tienen conocimiento que las otras redes existe y que hay hosts conectados a ellas. Por lo tanto no pueden resolver las consultas.


