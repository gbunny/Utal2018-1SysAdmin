## Running config

!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname ISP
!
!
!
enable secret 5 class
enable password cisco
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO1941/K9 sn FTX1524HPJB
!
!
!
!
!
!
!
!
!
no ip domain-lookup
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface GigabitEthernet0/0
 ip address 209.165.202.129 255.255.255.224
 duplex auto
 speed auto
!
interface GigabitEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/0/0
 no ip address
 clock rate 2000000
 shutdown
!
interface Serial0/0/1
 ip address 209.165.200.225 255.255.255.252
!
interface Vlan1
 no ip address
 shutdown
!
router rip
!
ip classless
ip route 192.168.1.64 255.255.255.192 Serial0/0/1 
ip route 10.10.2.0 255.255.254.0 Serial0/0/1 
!
ip flow-export version 9
!
!
!
banner motd ^C
********************************

!!!AUTHORIZED ACCESS ONLY!!!

********************************

^C
!
!
!
!
line con 0
 password cisco
 login
!
line aux 0
!
line vty 0 4
 password cisco
 login
!
!
!
end


## routing


Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2, E - EGP
       i - IS-IS, L1 - IS-IS level-1, L2 - IS-IS level-2, ia - IS-IS inter area
       * - candidate default, U - per-user static route, o - ODR
       P - periodic downloaded static route

Gateway of last resort is not set

     10.0.0.0/23 is subnetted, 1 subnets
S       10.10.2.0/23 is directly connected, Serial0/0/1
     192.168.1.0/26 is subnetted, 1 subnets
S       192.168.1.64/26 is directly connected, Serial0/0/1
     209.165.200.0/24 is variably subnetted, 2 subnets, 2 masks
C       209.165.200.224/30 is directly connected, Serial0/0/1
L       209.165.200.225/32 is directly connected, Serial0/0/1
     209.165.202.0/24 is variably subnetted, 2 subnets, 2 masks
C       209.165.202.128/27 is directly connected, GigabitEthernet0/0
L       209.165.202.129/32 is directly connected, GigabitEthernet0/0


## interface

Interface              IP-Address      OK? Method Status                Protocol 
GigabitEthernet0/0     209.165.202.129 YES manual up                    up 
GigabitEthernet0/1     unassigned      YES unset  administratively down down 
Serial0/0/0            unassigned      YES unset  administratively down down 
Serial0/0/1            209.165.200.225 YES manual up                    up 
Vlan1                  unassigned      YES unset  administratively down down
