# Laboratio 4: Rip
**Autor: Alan Lisboa**

## Topologia

![](./Topografia.png)

| Dispositivo | Interfaz | IP | Mascara de Subred | Gateway |
| --- | --- | --- | --- | --- |
| BRANCH | Gig 0/0 | 10.10.2.1 | 255.255.254.0 | N/C |
| | S0/0/0 | 192.168.1.62 | 255.255.255.192 | N/C |
| HQ | Gig 0/0 | 192.168.1.65 | 255.255.255.192 | N/C |
| | S0/0/1 | 209.165.200.226 | 255.255.255.252 | N/C |
| | S0/0/0 | 192.168.1.1 |255.255.255.192 | N/C |
| ISP | Gig 0/0 | 209.165.202.129 |255.255.255.224 | N/C |
| | S0/0/1 | 209.165.200.225 | 255.255.255.252 | N/C |
| PC1 | NIC | 10.10.3.254 | 255.255.254.0 | 10.10.2.1 |
| PC2 | NIC | 192.168.1.126 | 255.255.255.192 | 192.168.1.65 |
| PC3 | NIC | 209.165.202.158 | 255.255.255.224 | 209.165.202.129 |

## Tarea 1

### Paso 2

> ¿Cuántas subredes se deben crear de la red 192.168.1.0/24? 

- Se necesitan 2 subredes, una para la LAN HQ y otra para el enlace de HQ y BRANCH. LAN HQ Necesita 50 Host, se necesitan 6 bits minimo para diferenciarlos. Esta red es de 1/4 de C. Por eso se deben crear 4 subredes.

> ¿Cuál es la máscara de subred de esta red en formato decimal punteado?

- 255.255.255.192

> ¿Cuál es la máscara de subred de la red en formato de barra diagonal? 

- /26

> ¿Cuáles son las direcciones de red de las subredes?

- Subred 0: 192.168.1.0/26
- Subred 1: 192.168.1.64/26
- Subred 2: 192.168.1.128/26
- Subred 3: 192.168.1.192/26

> ¿Cuántas direcciones IP de hosts utilizables existen por subred? 

- 62 direcciones de host.

> ¿Cuántas direcciones IP host utilizables están disponibles en la LAN BRANCH?

- 510 direcciones Host, 2^(32 - 23) - 2  

## Tarea 2

### Paso 1

> 1. Asigne la primera dirección de host válida en la red 209.165.202.128/27 a la interfaz LAN del router ISP.

- 209.165.202.129

> 2. Asigne la última dirección de host válida en la red 209.165.202.128/27 para PC3.

- 209.165.202.158

> 3. Asigne la primera dirección de host válida en la red 209.165.200.224/30 para la interfaz WAN del router ISP.

- 209.165.200.225

> 4. Asigne la última dirección de host válida en la red 209.165.200.224/30 para la interfaz serial 0/0/1 del router HQ.

- 209.165.200.226

> 5. Asigne la primera dirección de host válida de la red LAN HQ para la interfaz LAN del router HQ.

- 192.168.1.65

> 6. Asigne la última dirección de host válida de la red LAN HQ para PC2.

- 192.168.1.126

> 7. Asigne la primera dirección de host válida en el enlace WAN HQ/BRANCH para la interfaz serial 0/0/0 del router HQ.

- 192.168.1.1

> 8. Asigne la última dirección de host válida en el enlace WAN HQ/BRANCH para la interfaz serial 0/0/0 del router BRANCH.

- 192.168.1.62

> 9. Asigne la primera dirección de host válida en la red 10.10.2.0/23 para la interfaz LAN en el router BRANCH.

- 10.10.2.1

> 10. Asigne la última dirección de host válida en la red 10.10.2.0/23 para PC1.

- 10.10.3.254

## Tarea 5

### Paso 1

```
BRANCH(config)#interface gig 0/0
BRANCH(config-if)#ip address 10.10.2.1 255.255.254.0
BRANCH(config-if)#no shutdown

BRANCH(config-if)#
%LINK-5-CHANGED: Interface GigabitEthernet0/0, changed state to up

%LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/0, changed state to up

BRANCH(config-if)#interface serial 0/0/0
BRANCH(config-if)#ip address 192.168.1.62 255.255.255.192
BRANCH(config-if)#no shutdown

%LINK-5-CHANGED: Interface Serial0/0/0, changed state to down
BRANCH(config-if)#

HQ#conf term
Enter configuration commands, one per line.  End with CNTL/Z.
HQ(config)#interface gig 0/0
HQ(config-if)#ip address 192.168.1.65 255.255.255.192
HQ(config-if)#no shutdown

HQ(config-if)#
%LINK-5-CHANGED: Interface GigabitEthernet0/0, changed state to up

%LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/0, changed state to up

HQ(config-if)#interface serial 0/0/1
HQ(config-if)#ip address 209.165.200.226 255.255.255.252
HQ(config-if)#no shutdown

%LINK-5-CHANGED: Interface Serial0/0/1, changed state to down
HQ(config-if)#no shutdown
HQ(config-if)#no shutdown
HQ(config-if)#interface serial 0/0/0
HQ(config-if)#ip address 192.168.1.1 255.255.255.192
HQ(config-if)#no shutdown

HQ(config-if)#
%LINK-5-CHANGED: Interface Serial0/0/0, changed state to up

HQ(config-if)#
%LINEPROTO-5-UPDOWN: Line protocol on Interface Serial0/0/0, changed state to up

HQ(config-if)#


ISP(config)#interface gig 0/0
ISP(config-if)#ip address 209.165.202.129 255.255.255.224
ISP(config-if)#no shutdown

 ISP(config-if)#
 %LINK-5-CHANGED: Interface GigabitEthernet0/0, changed state to up

 %LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/0, changed state to up

 ISP(config-if)#interface serial 0/0/1
 ISP(config-if)#ip address 209.165.200.225 255.255.255.252
 ISP(config-if)#no shutdown

```

### Paso 2

![](./PC1.png)

![](./PC2.png)

![](./PC3.png)

## Tarea 6

![](./T6.png)

## Tarea7



> ¿Qué redes están actualmente presentes en la tabla de enrutamiento de BRANCH antes de configurar RIP? Enumere las redes con notación de barra diagonal.

- 10.10.2.1/23
- 192.168.1.62/26

> ¿Qué comandos se requieren para habilitar la versión 1 de RIP e incluir estas redes en las actualizaciones de enrutamiento?

- router rip
- network ip

> ¿Hay alguna interfaz de router que no necesita recibir actualizaciones RIP?

- HQ S0/0/1

> ¿Qué comando se utiliza para deshabilitar las actualizaciones RIP de esta interfaz?

- passive-interface S0/0/1


## Tarea 8

> ¿Qué redes se encuentran en la tabla de enrutamiento de HQ? Enumere las redes con notación de barra diagonal.

- 192.168.1.64/26
- 209.165.200.226/30
- 192.168.1.1/26

> Será necesario configurar una ruta estática predeterminada para enviar todos los paquetes con direcciones de destino que no están en la tabla de enrutamiento del router ISP. ¿Qué comando se necesita para realizar esto? Utilice la interfaz de salida correspondiente en el router HQ en el comando.

- ip route 0.0.0.0 0.0.0.0 serial 0/0/1

> ¿Qué comandos se requieren para habilitar RIPv1 e incluir la red LAN en las actualizaciones de enrutamiento?

- router rip
- network 192.168.1.65

> ¿Hay alguna interfaz de router que no necesita recibir actualizaciones RIP? 

- S0/0/1

> ¿Qué comando se utiliza para deshabilitar las actualizaciones RIP de esta interfaz?

- passive-interface serial 0/0/1

> El router HQ necesita enviar la información de la ruta predeterminada al router BRANCH en las actualizaciones RIP. ¿Qué comando se utiliza para configurar esto?

- default-information originate

## Tarea 9

> Tarea 9: Configurar el enrutamiento estático en el router ISP.
Se deberán configurar rutas estáticas en el router ISP para todo el tráfico destinado a las direcciones RFC 1918 que se utilizan en la LAN BRANCH, LAN HQ y en el enlace entre los routers BRANCH y HQ. ¿Qué comandos necesitará configurar en el router ISP para lograr esto?

- ip route 10.10.2.0 255.255.254.0 serial 0/0/1
- ip route 192.168.1.64 255.255.255.192 serial 0/0/1

## Tarea 10

> ¿Es posible hacer ping en PC1 desde PC2?

- Si

> ¿Es posible hacer ping en PC3 desde PC2?

- Si

> ¿Es posible realizar un ping desde PC1 a PC3? 

- Si

> ¿Qué rutas se encuentran en la tabla de enrutamiento del router BRANCH?

![](./T101.png)

> ¿Cuál es el gateway de último recurso en la tabla de enrutamiento del router BRANCH?

- R *   0.0.0.0/0 [120/1] via 192.168.1.1, 00:00:21, Serial0/0/0

> ¿Qué rutas se encuentran en la tabla de enrutamiento del router HQ?

![](./T102.png)

> ¿Qué rutas están presentes en la tabla de enrutamiento del router ISP?

![](./T103.png)

> ¿Qué redes, incluyendo la métrica, están presentes en las actualizaciones RIP enviadas desde el router HQ?

- 192.168.1.64/27
- 192.168.1.0/27

> ¿Qué redes, incluyendo la métrica, están presentes en las actualizaciones RIP enviadas desde el router BRANCH?

- 10.10.2.0/23
- 192.168.1.0/27

## Tarea 11

> Si se hubiera utilizado enrutamiento estático en lugar de RIP en el router BRANCH, ¿cuántas rutas estáticas individuales se necesitarían para hosts en la LAN BRANCH para comunicarse con todas las redes en el Diagrama de topología?

- 8 rutas.





