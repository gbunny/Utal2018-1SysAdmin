# Proyecto Final Unidad 2
## Daniel Pavez, Alan Lisboa

> 1. Verificar la posibilidad de configurar RAID 10 en el entorno de virtualización y explicar por qué se escoge este tipo de configuración por encima de otra. De no ser posible configurarlo, debe explicar los motivos y que necesita para poder desplegarlo.

RAID 10 No se puede configurar en la plataforma de virtualización ya que este funcionar sobre el sitema de archivos de Linux, por lo cual la configuración logica de los discos ya se ha hecho y no es posible modificarla desde es sistema de virtualización.

> 2. Instalar, configurar y optimizar los servicios de Apache, Php y Mysql y desplegar una aplicación en este servidor.

![](./T0201.png)

![](./T0202.png)

> 3. Generar carga de trabajo con Jmeter pensando en 300 usuario concurrentes en el sistema que se conectan cada 3 segundos. Esta carga implica que el usuario se debe autentificar en la plataforma. Haga un análisis del rendimiento de la máquina con y sin carga.

![](./JMeter1.png)
![](./JMeter2.png)
![](./JMeter3.png)
![](./JMeter4.png)
![](./JMeter5.png)

- Servidor con carga

![](./HeavyLoad.png)

> 4. Realice la automatización de algunos procesos. Por ejemplo: Actualización todos los domingos a las 9:00 horas y respaldo de la base de Datos todos los días a las 12:00. Para este ejercicio valore la posibilidad de utilizar tareas programas en cron.

- `/usr/bin/autoupdate.sh`
```
apt-get update
apt-get upgrade
```
- `/usr/bin/autobackup.sh`
```
TIME=$(date +%Y%m$d)
mysqldump wpdb > wpdb$TIME.sql
```
- `# crontab -e `
```
0 9 * * 6 /usr/bin/autoupdate.sh
0 12 * * * /usr/bin/autobackup.sh
```

## Hardening de Servicios

### APACHE2
> Quitar Banner

![](./SimpleServerSignature.png)
![](./SimpleServerSignatureHeader.png)

> Deshabilitar browser listing

![](./NoLista.png)
![](./NoIndices.png)

> Eliminar Etag

![](./Etagconf.png)
![](./EtagGone.png)

> Que apache se ejecute con cuenta no privilegiada

![](./ApacheUserConf.png)
![](./ApacheUser.png)

> Proteger Directorio de Binarios y Configuracion de Servicios

`# adduser apache; adduser -g apache apache; chown -R apache:apache /etc/apache2/ `

![](./chown2.png)
 
> Protección de System Settings

![](./ProtectSettings.png)

> Permitir Solo Métodos GET POST y HEAD

![](./SoloHEAD.png)

> Deshabilitar Protocolo HTTP 1.0

![](./SOLOTTP11.png) 

> Pruebas Mediante OWASP ZAP

- Apache2 pagina simple sin hardening
![](./Hardening1.png)

- Despues de Hardening
![](./Hardening2.png)


### MARIADB
> Permitir solo conecciones desde la maquina local, a través de configuración de my.cnf

![](./BindIP.png)

> No permitir cargar archivos locales

![](./MariaDB2.png)

> Validar que log solo sean leídos por dueño y grupo


> Comprobacion OWASP

![](./Hardening3.png)


### Wordpress Example

> Antes de hardening

![](./Hardening4.png)

> Despues de hardening

![](./Hardening5.png)


## Configuraciones Adicionales

### DNS

- Instalar bind: ` # apt-get install bind9 `
- Abrir /etc/bind/named.conf.options, descomentar forwarders y añadir algun DNS

![](./DNS1.png)

- Reiniciar el servicio de bind9: ` # service bind9 restart `

Para probar DNS

- Abrir /ect/resolv.conf y editar la ip de nameserver a 127.0.0.1 (servicio DNS nuestro)

![](./DNS2.png)

- Realizar ping

![](./DNS3.png)


### FTP

- Instalar vsftpd: ` # apt-get install vsftpd `

- Editar el archivo de configuracion de vsftpd,` /etc/vsftpd.conf `

![](./FTP1.png)

![](./FTP2.png)

- Reiniciar el servicio: ` # service vsftpd restart `

Para probar el FTP

- Añadir un usuario: ` # adduser adidas `
- Modificar permisos del usuario ` # chmod a-w /home/adidas `
- Crear un directoro para poder subir archvios ` # mkdir /home/adidas/files && chown adidas:adidas /home/adidas/files `
- Intentar ingreso FTP mediante navegador

![](./FTP3.png)

![](./FTP4.png)

### SSH

- Instalar servidor ssh: ` # apt-get install openssh-server `
- Abrir `/etc/ssh/sshd_config`
- Añadir (despues de Port) intentos maximos de autenticación, para evitar ataques de fuerza bruta

![](./SSH1.png)

- Añadir los usuarios permitidos al final del archivo

![](./SSH2.png)

Probar SSH

![](./SSH3.png)

![](./SSH4.png)

